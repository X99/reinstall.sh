#!/bin/zsh

###############################################################################
# Useful functions
###############################################################################

# Enable colors
reset="$(tput sgr 0)"
red="$(tput setaf 1)"
green="$(tput setaf 2)"
bold="$(tput bold)"
clear="$(tput clear)"
skipped="skipped "

# variable containing sudo password
pw=""

dry_run=false

# this functions is the core of reinstall.sh
# It takes a command and its parameters as input, runs the command and
# depending on its success or failure, respectively prints a green check
# or a red cross. In the latter case, it will also log its output and the
# command that failed.
run() {
    output=$(eval "$1" 2>&1)
    if [ $? -eq 0 ]; then
        printf "%s✔%s" "${green}${bold}" "${reset}" # discard output
    else
        printf "%s✘%s" "${red}${bold}" "${reset}" # discard output
        {
            printf "Error:\n"
            printf "Command: %s\n" "$1"
            printf "Error message: %s\n\n" "$output"
        } >> log.txt
    fi
}

# confirm function, to be used later on
confirm () {
    # call with a prompt string or use a default
    read -q "response?${1:-Are you sure? [y/N]} "
    case "$response" in
        ([yY][eE][sS]|[yY]) true ;;
        (*)                 false ;;
    esac
}

# Checks whether a command exists in current environment or not.
is_command() {
    command -v "${1:?No command specified}" >/dev/null 2>&1
}

# formats an amount of seconds and prints it.
print_seconds() {
    secs=$1
    if [ "$secs" -ge 3600 ]; then
        printf '%02dh %02dm %02ds\n' $((secs/3600)) $((secs%3600/60)) $((secs%60))
    else
        printf '%02dm %02ds\n' $((secs%3600/60)) $((secs%60))
    fi
}

ask_for_sudo_password() {
	printf 'I will ask once for all your sudo password.\nMost packages do no need it, but a few will.\nAsking it now prevents the script from being paused.\n\n'
	read -s 'pw?Sudo password: '
	printf '\n\n'
	#echo $sudoPW | sudo -S yum update
}


while getopts ":d" opt; do
    case ${opt} in
    d ) # dry run
        echo "RUNNING IN DRY RUN MODE"
        dry_run=true
    ;;

    \? )
        echo "Usage: reinstall [-d]"
        echo " -d : dry run, will not install anything nor change any parameter."
        exit 1
    ;;
    esac
done

# parse settings file
source ./settings.sh

# check external values, set to empty value if not defined
if [ -z "${apps+1}" ]; then
    printf "%sWARNING%s %s\n" "${red}${bold}" "${reset}" "apps variable not set in settings file, setting to empty array."
    apps=()
fi
if [ -z "${cli_tools+1}" ]; then
    printf "%sWARNING%s %s\n" "${red}${bold}" "${reset}" "cli_tools variable not set in settings file, setting to empty array."
    cli_tools=()
fi


###############################################################################
# Beginning of the reinstall script.
###############################################################################
cat <<END
${clear}${red}
                        88                                               88  88                  88
                        ""                            ,d                 88  88                  88
                                                      88                 88  88                  88
8b,dPPYba,   ,adPPYba,  88  8b,dPPYba,   ,adPPYba,  MM88MMM  ,adPPYYba,  88  88       ,adPPYba,  88,dPPYba,
88P'   "Y8  a8P_____88  88  88P'   \`"8a  I8[    ""    88     ""     \`Y8  88  88       I8[    ""  88P'    "8a
88          8PP"""""""  88  88       88   \`"Y8ba,     88     ,adPPPPP88  88  88        \`"Y8ba,   88       88
88          "8b,   ,aa  88  88       88  aa    ]8I    88,    88,    ,88  88  88  888  aa    ]8I  88       88
88           \`"Ybbd8"'  88  88       88  \`"YbbdP"'    "Y888  \`"8bbdP"Y8  88  88  888  \`"YbbdP"'  88       88


${reset}
END

# Asks for user confirmation
confirm "This script will reinstall all of your stuff. Continue? [y/n]" || exit
printf '\n\n'
start=$(date +%s)

# prevent system from sleeping
caffeinate -s -w $$ &

# truncate log to zero
:> log.txt

# ask for password once for all. The aim is to retain it for as long as possible so
# user won't have to type it again.
ask_for_sudo_password

printf "OK, now sit back and relax, this is going to take a while ☕️\n"

printf "\n%s" "Disabling Gatekeeper..."
run 'echo $pw | sudo -S spctl --master-disable'

printf "\n%s" "Disabling quarantine..."
run 'defaults write com.apple.LaunchServices LSQuarantine -bool NO'

# install and update brew
printf "\n%s" "Installing Homebrew..."
if ! is_command brew; then
    run 'curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh -o install.sh'
    run 'yes "" | /bin/bash -c install.sh'
    run 'brew doctor'
    run 'rm -f install.sh'
else
    printf "%s%s%s" "${green}" "${skipped}" "${reset}"
fi

printf "\n%s" "Installing Homebrew Cask Upgrade..."
run 'brew tap buo/cask-upgrade'

# git conf
printf "\n%s" "Setting up git globals..."
run 'git config --global user.name "deadbird"'
run 'git config --global user.email "deadbird99@gmail.com"'

printf "\n%s" "Setting up python packages..."
if ! is_command python3; then
    run 'brew install python3'
else
    printf "%s%s%s" "${green}" "${skipped}" "${reset}"
fi
run 'echo $pw | sudo -S pip3 install --upgrade pip setuptools wheel pyenv'

printf "\n%s" "Setting up python3 as default..."
run 'python_version=$(python3 -V | cut -d" "" -f2)'
run 'pyenv install $python_version'
run 'pyenv global $python_version'

# Install many apps...
printf "\n%s" "Installing apps..."
for app in "${apps[@]}"; do
    printf "\n  Installing %s%s%s..." "${bold}" "${app}" "${reset}"
    run 'brew install --cask "$app"'
    done

# Install apps that need password
printf "\n%s" "Installing apps with password..."
for app in "${apps[@]}"; do
    printf "\n  Installing %s%s%s..." "${bold}" "${app}" "${reset}"
    run 'echo $pw | brew install --cask "$app"'
    done

printf "\n%s" "Installing SILABS Driver..."
run 'brew install --cask homebrew/cask-drivers/silicon-labs-vcp-driver'
run 'cp /usr/local/Caskroom/silicon-labs-vcp-driver/*/Troubleshooting/*.pkg .'
run 'echo $pw | sudo -S installer -pkg Silicon\ Labs\ VCP\ Driver.pkg -target /'
run 'rm Silicon\ Labs\ VCP\ Driver.pkg'

# Installing mosquitto (particular case, it's a service)
printf "\n%s" "Installing mosquitto server and NodeRed..."
run 'brew install nodejs'
run 'brew install mosquitto'
run 'brew services start mosquitto'
run 'echo $pw | sudo -S npm install -g --unsafe-perm node-red'
run 'echo $pw | sudo -S npm install -g pm2'
run 'pm2 start $(which node-red) -- -v'

open "/Applications/Alfred 4.app"
open "/Applications/Keka.app"

printf "\n%s" "Installing CLI tools..."
for tool in "${cli_tools[@]}"; do
    printf "\n  Installing %s..." "$tool"
    if ! is_command "$tool"; then
        run 'brew install "$tool"'
    else
        printf "%s%s%s" "${green}" "${skipped}" "${reset}"
    fi
done

# Install a few fonts
printf "\n%s" "Installing fonts..."
run 'brew tap homebrew/cask-fonts'
for font in "${fonts[@]}"; do
    printf "\n  Installing %s%s%s..." "${bold}" "${font}" "${reset}"
    run 'brew install --cask "$font"'
done

printf "\n%s" "Installing App Store apps..."
for macapp in "${macapps[@]}"; do
    printf "\n  Installing %s%s%s..." "${bold}" "${macapp}" "${reset}"
    run 'mas lucky "$macapp"'
    done

if [ ! -d "/Applications/pCloud Drive.app" ]; then
    printf "\n%s" "Installing pCloud..."

	# get API code. The result is a web from from which we will parse out the API code.
	apicode=$(curl -s https://www.pcloud.com/how-to-install-pcloud-drive-mac-os.html\?download\=mac | grep "'Mac':" | sed "s/[ ,:']*//g;s/Mac//g" | tr -d '\t')

	# compose url
	url="https://api.pcloud.com/getpublinkdownload?code=${apicode}"

	# get path from which we will download pcloud.
	p=$(curl -s ${url} | grep path | sed 's/path//g;s/[" :,]*//g;s|\\||g' | tr -d '\t')

	# get a host. There might be more than one, get the first one.
	h=$(curl -s ${url} | grep .pcloud.com | head -1 | sed 's/[" ,]*//g' | tr -d '\t')

	# retrieve pcloud
	u="https://${h}${p}"
	run "curl -s $u -o pcloud.pkg"

	#install pCloud
	run 'echo $pw | sudo -S installer -pkg pcloud.pkg -target /'
fi

printf "\n%s" "Setting up the dock..."
run 'dockutil --add "$HOME/Desktop" --view auto --display folder --replacing "Desktop"'
run 'dockutil --add "$HOME/Documents" --view auto --display folder --replacing "Documents"'
run 'dockutil --remove Safari'
run 'dockutil --remove TV'
run 'dockutil --remove Podcasts'
run 'dockutil --remove Contacts'
run 'dockutil --remove Calendar'
run 'dockutil --remove Notes'
run 'dockutil --remove Reminders'
run 'dockutil --remove Maps'
run 'dockutil --remove Photos'
run 'dockutil --remove FaceTime'
run 'dockutil --remove iBooks'
run 'm dock autohide YES'
run 'm finder showpath YES'
run 'm finder showextensions YES'

# TODO: make a dictionary out of these associations
printf "\n%s" "Setting up file associations..."
run 'duti -s com.microsoft.VSCode .h all'
run 'duti -s com.microsoft.VSCode .cpp all'
run 'duti -s com.microsoft.VSCode .c all'
run 'duti -s com.microsoft.VSCode .md all'
run 'duti -s com.microsoft.VSCode .sh all'
run 'duti -s com.microsoft.VSCode .ini all'
run 'duti -s com.microsoft.VSCode .py all'

printf "\nCreating repos directories..."
run 'mkdir "$HOME/Documents/Repositories"'
run 'ln -s "$HOME/Documents/Repositories/" "$HOME/repos"'

printf "\n%s" "Deactivating guest account..."
run 'echo $pw | sudo -S /usr/bin/defaults write /Library/Preferences/com.apple.loginwindow GuestEnabled -bool NO'

printf "\n%s" "Configuring Finder..."
run 'defaults write com.apple.finder ShowPathbar -bool true'
run 'defaults write com.apple.finder ShowStatusBar -bool true'
run 'defaults write -g NSDocumentSaveNewDocumentsToCloud -bool false'
run 'defaults write com.apple.finder QLEnableTextSelection -bool TRUE'
run 'echo $pw | sudo -S defaults write /Library/Preferences/com.apple.loginwindow LoginwindowText "Found this computer? Contact me at deadbird99@gmail.com."'

# install laptop/desktop specifics
#if system_profiler SPHardwareDataType | grep iMac; then
#fi

printf "\n%s" "Making CLI awesome..."

printf "\n  %s" "Installing oh-my-zsh..."
if [ ! -d "$HOME/.oh-my-zsh" ]; then
    run 'curl -fsSLO https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh'
    run 'sed -i "" "s/exec zsh -l//g" install.sh'
    run 'chmod +x ./install.sh'
    run 'sh -c ./install.sh'
    run 'rm -f install.sh'
fi

ZSH_CUSTOM=$HOME/.oh-my-zsh/custom

printf "\n  %s" "Installing zsh-syntax-highlighting..."
if [ ! -d "$ZSH_CUSTOM/plugins/zsh-syntax-highlighting" ]; then
    run 'git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "$ZSH_CUSTOM/plugins/zsh-syntax-highlighting"'
else
    printf "%s%s%s" "${green}" "${skipped}" "${reset}"
fi

printf "\n  %s" "Installing zsh-autosuggestions..."
if [ ! -d "$ZSH_CUSTOM/plugins/zsh-autosuggestions" ]; then
    run 'git clone https://github.com/zsh-users/zsh-autosuggestions "$ZSH_CUSTOM/plugins/zsh-autosuggestions"'
else
    printf "%s%s%s" "${green}" "${skipped}" "${reset}"
fi

printf "\n  %s" "Installing powerlevel10k..."
run 'git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel10k'

printf "\n  %s" "Installing zsh-completions..."
if [ ! -d "$HOME/.oh-my-zsh/custom/plugins/zsh-completions" ]; then
    run 'git clone https://github.com/zsh-users/zsh-completions "$HOME/.oh-my-zsh/custom/plugins/zsh-completions"'
else
    printf "%s%s%s" "${green}" "${skipped}" "${reset}"
fi

# Retrieveing zshrc from repo
printf "\n  %s" "Retrieveing custom .zshrc..."
run 'git clone https://gitlab.com/X99/zshrc.git ~/Documents/Repositories/zshrc'
run 'mv ~/Documents/Repositories/zshrc/.zshrc ~'
run 'rm -rf ~/Documents/Repositories/zshrc'
echo 'export PATH="/usr/local/sbin:$PATH"' >> ~/.zshrc

# installing colorls
if ! is_command colorls > /dev/null; then
    printf "\n  %s" "Installing colorls..."
    run 'echo $pw | sudo gem install colorls'
else
    printf "%s%s%s" "${green}" "${skipped}" "${reset}"
fi

printf "\n%s" "Restoring settings..."
run 'git clone https://gitlab.com/X99/dotfiles.git ~/Documents/Repositories/dotfiles'
run 'chmod +x "$HOME/Documents/Repositories/dotfiles/dotfiles"'
run '$HOME/Documents/Repositories/dotfiles/dotfiles -r'

printf '\n\n\n'
confirm "Now entering copy/paste phase, plase make sure Alfred is ready to go."

printf "\n%s" "Preaparing Gitlab token for Alfred..."
run 'cat $HOME/Documents/Repositories/dotfiles/gitlab_key | pbcopy'
printf '\n'
confirm "Press enter when you are done pasting it to Alfred"

printf "\n%s" "Preaparing Brave sync code..."
run 'cat $HOME/Documents/Repositories/dotfiles/brave_sync_code | pbcopy'
printf '\n'
confirm "Press enter when you are done pasting it to Brave"


# removes clipboard content
cat /dev/null | pbcopy


printf "\n%s" "Cleaning up..."
run 'brew cleanup'
run 'rm pcloud.pkg'

end=$(date +%s)
runtime=$((end-start))

printf "\n\n\nDone in %s ! Have fun!\n\n\n\n" "$(print_seconds ${runtime})"
say -v Samantha "Reinstallation complete"
